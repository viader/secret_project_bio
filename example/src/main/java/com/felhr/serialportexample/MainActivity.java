package com.felhr.serialportexample;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.ref.WeakReference;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

  UsbService usbService;

  public TextView display;

  private MyHandler mHandler = new MyHandler(this);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Button sendButton = (Button) findViewById(R.id.buttonSend);
    sendButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
                /*if (!editText.getText().toString().equals("")) {

                    String data = buffer.toString();

                    //String data = buffer.toString().getBytes();
                    //String data = editText.getText().toString();
                    openActivity(data);*/
        //if (usbService != null) { // if UsbService was correctly binded, Send data

        display = (TextView) findViewById(R.id.textView);
        String data = null;
        display.append(data);
        //
        usbService.write(data.getBytes());
        openActivity(data);
        //}
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    setFilters();  // Start listening notifications from UsbService
    startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
  }

  @Override
  public void onPause() {
    super.onPause();
    unregisterReceiver(usbReceiver);
    unbindService(usbConnection);
  }

  /*
   * Notifications from UsbService will be received here.
   */
  private final BroadcastReceiver usbReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      switch (intent.getAction()) {
        case C.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
          Toast.makeText(context, "USB Ready", Toast.LENGTH_SHORT).show();
          break;
        case C.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
          Toast.makeText(context, "USB Permission not granted", Toast.LENGTH_SHORT).show();
          break;
        case C.ACTION_NO_USB: // NO USB CONNECTED
          Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
          break;
        case C.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
          Toast.makeText(context, "USB disconnected", Toast.LENGTH_SHORT).show();
          break;
        case C.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
          Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
          break;
      }
    }
  };

  private final ServiceConnection usbConnection = new ServiceConnection() {
    @Override
    public void onServiceConnected(ComponentName arg0, IBinder arg1) {
      usbService = ((UsbService.UsbBinder) arg1).getService();
      usbService.setHandler(mHandler);
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
      usbService = null;
    }
  };

  /*
   * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
   */
  private static class MyHandler extends Handler {
    private final WeakReference<MainActivity> mActivity;

    public MyHandler(MainActivity activity) {
      mActivity = new WeakReference<>(activity);
    }

    @Override
    public void handleMessage(Message msg) {
      Log.e(C.LOG_TAG, "MyHandler handleMessage");
      Log.e(C.LOG_TAG, msg.toString());
      switch (msg.what) {
        case C.MESSAGE_FROM_SERIAL_PORT:
          String data = (String) msg.obj;
          mActivity.get().openActivity(data);
          break;
      }
    }
  }

  public void openActivity(String data) {
    Intent intent = new Intent(this, ImageActivity.class);
    switch (data) {      // open new activity with image
      case "a":
        intent.putExtra(C.INTENT_KEY_IMAGE, "/PICTURES/jelly.jpg");
        break;
      case "b":
        intent.putExtra(C.INTENT_KEY_IMAGE, "/PICTURES/b.jpg");
        break;
      case "c":
        intent.putExtra(C.INTENT_KEY_IMAGE, "/PICTURES/c.jpg");
        break;
      case "d":
        intent.putExtra(C.INTENT_KEY_IMAGE, "/PICTURES/d.jpg");
        break;
      case "e":
        intent.putExtra(C.INTENT_KEY_IMAGE, "/PICTURES/e.jpg");
        break;
      default:
        return;
    }
    intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
    startActivity(intent);
  }

  private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
    Intent startService = new Intent(this, service);
    if (extras != null && !extras.isEmpty()) {
      Set<String> keys = extras.keySet();
      for (String key : keys) {
        String extra = extras.getString(key);
        startService.putExtra(key, extra);
      }
    }
    startService(startService);

    Intent bindingIntent = new Intent(this, service);
    bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
  }

  private void setFilters() {
    IntentFilter filter = new IntentFilter();
    filter.addAction(C.ACTION_USB_PERMISSION_GRANTED);
    filter.addAction(C.ACTION_NO_USB);
    filter.addAction(C.ACTION_USB_DISCONNECTED);
    filter.addAction(C.ACTION_USB_NOT_SUPPORTED);
    filter.addAction(C.ACTION_USB_PERMISSION_NOT_GRANTED);
    registerReceiver(usbReceiver, filter);
  }
}
