package com.felhr.serialportexample;

public interface C {

  String ACTION_USB_READY = "com.felhr.connectivityservices.USB_READY";

  String ACTION_USB_ATTACHED = "android.hardware.usb.action.USB_DEVICE_ATTACHED";

  String ACTION_USB_DETACHED = "android.hardware.usb.action.USB_DEVICE_DETACHED";

  String ACTION_USB_NOT_SUPPORTED = "com.felhr.usbservice.USB_NOT_SUPPORTED";

  String ACTION_NO_USB = "com.felhr.usbservice.NO_USB";

  String ACTION_USB_PERMISSION_GRANTED = "com.felhr.usbservice.USB_PERMISSION_GRANTED";

  String ACTION_USB_PERMISSION_NOT_GRANTED = "com.felhr.usbservice.USB_PERMISSION_NOT_GRANTED";

  String ACTION_USB_DISCONNECTED = "com.felhr.usbservice.USB_DISCONNECTED";

  String ACTION_CDC_DRIVER_NOT_WORKING = "com.felhr.connectivityservices.ACTION_CDC_DRIVER_NOT_WORKING";

  String ACTION_USB_DEVICE_NOT_WORKING = "com.felhr.connectivityservices.ACTION_USB_DEVICE_NOT_WORKING";

  int MESSAGE_FROM_SERIAL_PORT = 0;

  String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

  int BAUD_RATE = 9600; // BaudRate. Change this value if you need

  String LOG_TAG = "SerialPortExample";

  String INTENT_KEY_IMAGE = "INTENT_KEY_IMAGE";
}
