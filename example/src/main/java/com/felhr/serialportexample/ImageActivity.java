package com.felhr.serialportexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ImageActivity extends AppCompatActivity {

  //public ImageView ImageView2 = (ImageView) findViewById(R.id.imageView2);
  //private Bitmap currentImage;
  //Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_image);

    String imageFilepath = getIntent().getStringExtra(C.INTENT_KEY_IMAGE);

    File sd = Environment.getExternalStorageDirectory();
    File image = new File(sd + imageFilepath);

    if (image.exists()) {

      BitmapFactory.Options bmOptions = new BitmapFactory.Options();
      Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);

      //bitmap = Bitmap.createScaledBitmap(bitmap, , parent.getHigh(), true);
      ImageView ImageView2 = (ImageView) findViewById(R.id.imageView2);
      ImageView2.setImageBitmap(bitmap);
    } else {

      Context context = getApplicationContext();
      Toast.makeText(context, "Can't find a picture", Toast.LENGTH_SHORT).show();
    }
  }
}




